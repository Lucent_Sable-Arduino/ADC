/*
 * sinefn.c
 *
 * A program to generate a sine wave using the DCA module
 *
 */

//included for delay functions
#include <util/delay.h>

#include <avr/io.h>

//included for sprintf
#include <stdio.h>

//included for LCD functions
#include <lcd.h>
#include "adc.h"

int main(void)
{
	char buff[40];
	int val;
	int volt, mil_volt;

	lcd_init();

	adc_init(0);

	while(1)
	{
		val = adc_read();
		mil_volt = (5*val);
		volt = mil_volt / 1023;
		mil_volt = mil_volt % 1023;
		sprintf(buff,"ADC:%d\nVoltage:%d.%03d", val, volt, mil_volt);
		lcd_display(buff);
		_delay_ms(700);
	}

}

